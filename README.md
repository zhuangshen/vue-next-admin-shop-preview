<div align="center">
	<img src="https://i.hd-r.cn/fc3fc574c61b5436dffa2883317a75c3.png">
	<p align="center">
	    <a href="https://v3.vuejs.org/" target="_blank">
	        <img src="https://img.shields.io/badge/vue.js-vue3.x-green" alt="vue">
	    </a>
	    <a href="https://www.dcloud.io/index.html" target="_blank">
	        <img src="https://img.shields.io/badge/HBuilderX-%3E3.2.15-blue" alt="HBuilderX">
	    </a>
		<a href="https://vitejs.cn/vite3-cn/" target="_blank">
		    <img src="https://img.shields.io/badge/vite-3.2.4-yellow" alt="vite">
		</a>
		<a href="https://vitejs.dev/" target="_blank">
		    <img src="https://img.shields.io/badge/license-MIT-success" alt="vite">
		</a>
	</p>
	<p>&nbsp;</p>
</div>

#### 💝 长期赞助商

<a href="http://www.ccflow.org/" target="_blank">
	<img src="./ccflowRightNextAdmin.png" width="50%" height="70px">
</a>

#### 🌈 介绍

🎉🎉🔥 基于 uni-app vue3.x setup 语法糖，一套代码编到 14 个平台商城项目模板预览库

#### ⛱️ 线上预览

- H5（Ctrl + Shift + I、Ctrl + Shift + M）：<a href="https://lyt-top.gitee.io/vue-next-admin-shop-preview" target="_blank">https://lyt-top.gitee.io/vue-next-admin-shop-preview</a>
